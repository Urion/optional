import * as deepEqual from 'deep-equal'

/**
 * This library is made to define Optional type in typescript.
 * The Optional type could be None or Some
 */
export type Optional<A> = None<A> | Some<A>


export class Some<A> {
	value
	
	constructor(value) {
		this.value = value 
	}
	
	getOrUndefined(): A {
		return this.value
	}

	getOrElse<A>(defaultValue: A): A {
		return this.value
	}

	map<B>(f: (a: A) => B): Optional<B> {
		return new Some(f(this.value))
	}

	flatMap<B>(f: (a: A) => Optional<B>): Optional<B> {
		return new Some(f(this.value))
	}

	isDefined(): boolean {
		return true
	}

	nonEmpty(): boolean {
		return true
	}

	isEmpty(): boolean {
		return false
	}

	orElse<A>(maybeA: Optional<A>): Optional<A> {
		return new Some(this.value)
	}

	orNull(): A {
		return this.value
	}

	exists<A>(f: (a: A) => boolean): boolean {
		return f(this.value)
	}

	contains(a: A): boolean {
		return deepEqual(this.value, a)
	}

	toString(): String {
		return `Some(${this.value.toString()})`
	}

	fold<B>(isEmpty: B, f: (a: A) => B): B {
		return f(this.value)
	}
}

export class None<A> {

	static instance : Optional<never> = new None()

	getOrUndefined(): A {
		return undefined
	}

	getOrElse<A>(defaultValue: A): A {
		return defaultValue
	}

	map<B>(f: (a: A) => B): Optional<B> {
		return none
	}

	flatMap<B>(f: (a: A) => Optional<B>): Optional<B> {
		return none
	}

	isDefined(): boolean {
		return false
	}

	nonEmpty(): boolean {
		return false
	}

	isEmpty(): boolean {
		return true
	}

	orElse<A>(maybeA: Optional<A>): Optional<A> {
		return maybeA
	}

	orNull(): A {
		return null
	}

	exists<A>(f: (a: A) => boolean): boolean {
		return false
	}

	contains(a: A): boolean {
		return false
	}

	toString(): String {
		return `none`
	}

	fold<B>(isEmpty: B, f: (a: A) => B): B {
		return isEmpty
	}
}

export const none = None.instance
