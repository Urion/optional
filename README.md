<h1> Optional </h1>

A new Typescript library that implements option and some of the most important function you would use with optional types.

<h2>Usage: </h2>

First of all install the dependency inside your project

```bash
npm i @urion/optional
```

Here an example of usage:

```javascript
    const maybeVar = new Some('Foo')
    const maybeNot = none

    maybeVar.isDefined() // return true
    maybeNot.isDefined() // return false

    maybeVar.isEmpty()   // return false
    maybeNot.isEmpty()   // return true

    maybeVar.getOrElse('Bar') // return 'Foo'
    maybeNot.getOrElse('Bar') // return 'Bar'
```

Feel free to create a pull request if you want to change something :)
